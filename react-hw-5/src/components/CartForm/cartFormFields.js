import style from './cartForm.module.scss'

export const cartFormFields = [
    {
        tagType: 'regular',
        label: "Ім'я користувача",
        labelClass: style.form__label,
        inputClass: style.form__input,
        errorClass: style.form__error,
        name: "firstName",
        type: "text",
        placeholder: "Тарас",
    },
    {
        tagType: 'regular',
        label: "Прізвище користувача",
        labelClass: style.form__label,
        inputClass: style.form__input,
        errorClass: style.form__error,
        name: "lastName",
        type: "text",
        placeholder: "Шевченко",
    },
    {
        tagType: 'regular',
        label: "Вік користувача",
        labelClass: style.form__label,
        inputClass: style.form__input,
        errorClass: style.form__error,
        name: "age",
        type: "number",
        placeholder: "42",
    },
    {
        tagType: 'regular',
        label: "Адреса доставки",
        labelClass: style.form__label,
        inputClass: style.form__input,
        errorClass: style.form__error,
        name: "address",
        type: "text",
        placeholder: "м.Харків ...",
    },
    {
        tagType: 'masked',
        label: "Мобільний телефон",
        labelClass: style.form__label,
        inputClass: style.form__input,
        errorClass: style.form__error,
        name: "phone",
        type: "string",
        format: "(###) ###-##-##",
        mask: "#",
    }
]
