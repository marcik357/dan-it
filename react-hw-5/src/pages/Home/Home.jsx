import Container from '../../components/Container';
import ProductList from '../../components/ProductList';
import { useSelector } from "react-redux";

export function Home() {

    const products = useSelector(state => state.products.products)
    const loading = useSelector(state => state.loading.loading)
    const error = useSelector(state => state.error.error)

    if (error) {
        return <p>Відбулась непередбаченна помилка при завантаженні, спробуйте оновити сторінку</p>;
    }

    return (
        <Container>
            {!loading
                ? <ProductList products={products} type={'buy'} />
                : <p>Йде завантаження...</p>}
        </Container>
    )
}