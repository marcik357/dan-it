import * as Yup from 'yup';

export const validationSchemaCart = Yup.object({
    firstName: Yup.string()
        .min(2, 'Має містити принаймні 2 літери')
        .max(25, 'Може бути не більше 25 символів')
        .trim()
        .required("Обов'язкове поле!"),
    lastName: Yup.string()
        .min(2, 'Має містити принаймні 2 літери')
        .max(25, 'Може бути не більше 25 символів')
        .trim()
        .required("Обов'язкове поле!"),
    age: Yup.number()
        .min(18, 'Вам має бути принаймні 18 років')
        .max(150, 'Люди ще не навчились жити так довго :)')
        .positive()
        .integer()
        .required("Обов'язкове поле!"),
    address: Yup.string()
        .min(3, 'Має містити принаймні 2 літери')
        .max(40, 'Може бути не більше 40 символів')
        .trim()
        .required("Обов'язкове поле!"),
    phone: Yup.string()
        .matches(/^[^#]*$/)
        .required("Обов'язкове поле!"),
})