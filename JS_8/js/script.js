//  1)
let allParagraph = document.querySelectorAll('p');

allParagraph.forEach(p => {
    p.style.backgroundColor = '#ff0000';
});

//  2)
let optionsList = document.querySelector('#optionsList');

console.log(optionsList);
console.log(optionsList.parentElement);
console.log(optionsList.childNodes);

if (optionsList.childNodes) {
    optionsList.childNodes.forEach(node => {
        let nodeType;
        if (node.nodeType === 1) {
            nodeType = 'Element node';
        } else if (node.nodeType === 3) {
            nodeType = 'Text node';
        } else if (node.nodeType === 8) {
            nodeType = 'Comment node';
        }
        console.log(`${node.nodeName} - ${nodeType}`);
    })
};

//  3)
let testParagraph = document.querySelector('#testParagraph');

testParagraph.textContent = 'This is a paragraph';

//  4)
let mainHeaderElements = document.querySelector('.main-header').children;

for (let elem of mainHeaderElements) {
    console.log(elem);
    elem.classList.add('nav-item');
};

//  5)
let sectionTitle = document.querySelectorAll('.section-title');

sectionTitle.forEach(elem => elem.classList.remove('section-title'));
