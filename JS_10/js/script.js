let buttonsList = document.querySelector('.tabs');

buttonsList.addEventListener('click', listHandler);

function listHandler(e) {
    if (e.target.closest('.tabs-title')) {
        let contentList = document.querySelector('.tabs-content');
        let title = e.target.closest('.tabs-title').dataset.title;

        Array.from(buttonsList.children).forEach(button => button.classList.remove('active'));
        e.target.closest('.tabs-title').classList.add('active');

        Array.from(contentList.children).forEach(tab => tab.dataset.name === title ? tab.classList.add('active') : tab.classList.remove('active'))
    }
}