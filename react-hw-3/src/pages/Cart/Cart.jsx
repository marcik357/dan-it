import { useOutletContext } from "react-router-dom";
import ProductList from '../../components/ProductList';

export function Cart() {

    const { cart, error, loading } = useOutletContext()

    if (error) {
        return <p>Відбулась непередбаченна помилка при завантаженні, спробуйте оновити сторінку</p>;
    }

    const uniqueArtNums = [...new Set(cart.map(item => item.artNum))]
    const uniqueCartProducts = uniqueArtNums.map(item => cart.find(product => product.artNum === item))

    return (
        <>
            {!loading
                ? cart.length
                    ? <ProductList products={uniqueCartProducts} type={'delete'} productAmount={true} />
                    : <p>Ви ще нічого не додали в кошик</p>
                : <p>Йде завантаження...</p>}
        </>
    )
}