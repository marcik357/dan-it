import { useOutletContext } from "react-router-dom";
import ProductList from '../../components/ProductList';

export function Favorites() {

    const { favorites, error, loading } = useOutletContext()

    if (error) {
        return <p>Відбулась непередбаченна помилка при завантаженні, спробуйте оновити сторінку</p>;
    }

    return (
        <>
            {!loading
                ? favorites.length
                    ? <ProductList products={favorites} type={'buy'} />
                    : <p>Ви ще нічого не додали в обране</p>
                : <p>Йде завантаження...</p>}
        </>
    )
}