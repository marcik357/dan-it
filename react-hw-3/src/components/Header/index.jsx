import style from './header.module.scss'
import PropTypes from 'prop-types';
import { FavoriteIcon, CartIcon } from "../icons";
import { Link } from 'react-router-dom';

function Header({ favorites = 0, cart = 0 }) {

    return (
        <>
            <header className={style.header}>
                <div className={style.header__container}>
                    <div className={style.header__wrapper}>
                        <Link to='/' className={style.header__logo}>
                            <img className={style.header__img} src="/logo.png" alt="logo" />
                        </Link>
                        <div className={style.header__btns}>
                            <Link to='/favorites' className={style.header__favorites}>
                                <FavoriteIcon />
                                {favorites > 0 && <div>{favorites}</div>}
                            </Link>
                            <Link to='/cart' className={style.header__cart}>
                                <CartIcon />
                                {cart > 0 && <div>{cart}</div>}
                            </Link>
                        </div>
                    </div>
                </div>
            </header>
        </>
    )
}

Header.propTypes = {
    favorites: PropTypes.number,
    cart: PropTypes.number
}

export default Header