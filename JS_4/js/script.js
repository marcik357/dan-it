let firstNum = prompt('Enter first number');
let secondNum = prompt('Enter second number');
let operator = prompt('Enter operator');

while (isNaN(+firstNum) || isNaN(+secondNum)) {
    alert('Please, enter VALID numbers');
    firstNum = prompt('Enter first number', firstNum);
    secondNum = prompt('Enter second number', secondNum);
}

while (operator != '+' && operator != '-' && operator != '*' && operator != '/') {
    alert('Please, enter VALID operator');
    operator = prompt('Enter operator', operator);
}

calc(firstNum, secondNum, operator);

function calc(a, b, operator) {
    let res;
    
    switch (operator) {
        case '+':
            res = +a + +b;
            break;
        case '-':
            res = a - b;
            break;
        case '*':
            res = a * b;
            break;
        case '/':
            if (+a === 0 || +b === 0) {
                alert('Division by zero is impossible')
            } else {
                res = a / b;
            }
            break;
    
        default:
            break;
    }
    console.log(res);
}