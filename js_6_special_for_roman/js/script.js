function createNewUser() {
    let newUser = {
        getLogin() {
            return (this.firstName.substring(0, 1) + this.lastName).toLowerCase();
        },
        getAge() {
            let age = new Date().getFullYear() - new Date(this.birthday.slice(-4), (this.birthday.slice(3, 5) - 1), this.birthday.slice(0, 2)).getFullYear();
            let month = new Date().getMonth() - (this.birthday.slice(3, 5) - 1);
            if (month < 0 || (month === 0 && new Date().getDate() < this.birthday.slice(0, 2))) {
                age++;
            }
            return age;
        },
        getPassword() {
            return this.firstName.substring(0, 1).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
        },
        setFirstName(newFirstName) {
            Object.defineProperty(this, 'firstName', {
                value: newFirstName,
            });
        },
        setLastName(newLastName) {
            Object.defineProperty(this, 'lastName', {
                value: newLastName,
            });
        },
        setBirthday(newBirthday) {
            Object.defineProperty(this, 'birthday', {
                value: newBirthday,
            });
        },
    };

    Object.defineProperty(newUser, 'firstName', {
        value: prompt('Enter user first name'),
        writable: false,
        configurable: true
    });
    Object.defineProperty(newUser, 'lastName', {
        value: prompt('Enter user last name'),
        writable: false,
        configurable: true
    });
    Object.defineProperty(newUser, 'birthday', {
        value: prompt('Please, enter user birthday in format dd.mm.yyyy'),
        writable: false,
        configurable: true
    });

    return newUser;
};

let myUser = createNewUser();
console.log(myUser);
console.log(myUser.getLogin());
console.log(myUser.getAge());
console.log(myUser.getPassword());