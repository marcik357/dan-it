window.addEventListener("scroll", headerChange);

function headerChange(e) {
    let header = document.querySelector(".header");
    if (window.scrollY > 10) header.classList.add("sticky"); else header.classList.remove("sticky");
}