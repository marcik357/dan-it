function cloneObject(obj) {
    let objClone;

    if (obj instanceof Object) {
        objClone = Array.isArray(obj) ? [] : {};
    
        if (Object.keys(obj).length) {
            for (let [key, value] of Object.entries(obj)) {
                objClone[key] = cloneObject(value);
            }
        }
        return objClone;
    } else {
        return obj;
    }
}

const exampleObj = {
    key11: "325",
    key12: 4,
    key_array: [
        [
            {
                obj_key1_in_array: 12,
                obj_key2_in_array: {
                    obj_key3_in_array: {
                        go_deeper: {
                            go_deeper_and_deeper: {
                                go_deeper_and_deeper_and_deeper: {
                                    go_deeper_and_deeper_and_deeper_and_deeper: [2, '214', undefined, null, true],
                                },
                            },
                        },
                    },
                },
                obj_key3_in_array: [2, '214', undefined, null, true],
            },
            {
                obj_key1_in_array: 12,
                obj_key2_in_array: {
                    obj_key1_in_array: 12,
                    obj_key2_in_array: 12,
                    obj_key3_in_array: [2, '214', undefined, null, true],
                },
                obj_key3_in_array: [2, '214', undefined, null, true],
            },
        ],
    ],
    key13: {
        obj_key1_in_array: 12,
        obj_key2_in_array: {
            obj_key1_in_array: 12,
            obj_key2_in_array: 12,
            obj_key3_in_array: [2, '214', {
                obj_key1_in_array: 12,
                obj_key2_in_array: 12,
                obj_key3_in_array: [2, '214', undefined, null, true],
            }, null, true],
        },
        obj_key3_in_array: [2, '214', undefined, null, true],
    },
};

console.log(exampleObj);
let newObj = cloneObject(exampleObj)
console.log(newObj);