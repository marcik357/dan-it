import './App.scss'
import Button from "./components/Button";
import { Component } from 'react'
import Modal from "./components/Modal";
import { modalProps } from './components/Modal/modalProps.jsx'

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modalOpen: null,
    }
  }

  openModalHandler = (e) => {
    const id = e.target.dataset.modalId
    this.setState({ modalOpen: id })
  }

  closeModalHandler = () => {
    this.setState({ modalOpen: null })
  }

  render() {
    return (
      <>
        {this.state.modalOpen
          && <Modal
            onClick={this.closeModalHandler}
            data={modalProps.find(modal => modal.id === this.state.modalOpen)}/>}
        <Button
          onClick={this.openModalHandler}
          dataModalId={'modalID1'}
          backgroundColor={'#E74C3C'}
          text={'Open first modal'}
        />
        <Button
          onClick={this.openModalHandler}
          dataModalId={'modalID2'}
          backgroundColor={'#3454c7'}
          text={'Open second modal'}
        />
      </>
    )
  }
}