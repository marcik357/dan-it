export const modalProps = [
    {
        id: 'modalID1',
        title: "Do you want to delete this file?",
        description: `Once you delete this file, it's won't be possible to undo this action. Are you sure you want to delete it?`,
        closeBtn: true,
        closeBtnHandler(closeHandler, className) {
            return (
                <button onClick={closeHandler} className={className}/>
            )
        },
        actions(closeHandler, submitHandler, className) {
            return (
                <div className={className}>
                    <button onClick={submitHandler} className='submit'>Ok</button>
                    <button onClick={closeHandler} className='cancel'>Cancel</button>
                </div>
            )
        },
    },
    {
        id: 'modalID2',
        title: "Додати в кошик",
        description: 'Ви дійсно хочете додати цей товар у ваш кошик?',
        closeBtn: false,
        closeBtnHandler(closeHandler, className) {
            return (
                <button onClick={closeHandler} className={className} />
            )
        },
        actions(closeHandler, submitHandler, className) {
            return (
                <div className={className}>
                    <button onClick={submitHandler} className='submit'>Додати</button>
                    <button onClick={closeHandler} className='cancel'>Відміна</button>
                </div>
            )
        },
    }
]
