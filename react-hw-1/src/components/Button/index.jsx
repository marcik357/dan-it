import style from './button.module.scss'
import { Component } from 'react'

export default class Button extends Component {

    
    render() {
        const { onClick, dataModalId, backgroundColor, text } = this.props

        return (
            <button
                onClick={onClick}
                data-modal-id={dataModalId}
                style={{backgroundColor: backgroundColor}}
                className={style.btnMain}
            >
                {text}
            </button>
        )
    }
}