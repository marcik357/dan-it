// Виведіть цей масив на екран у вигляді списку(тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id = "root", куди і потрібно буде додати цей список(схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price).
// Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const mustHaveProps = ['author', 'name', 'price']

function createHTMLElement(tagName = "div", className, value) {
    const elem = document.createElement(tagName);
    if (className) {
        elem.classList.add(className)
    }
    if (value !== undefined) {
        ['input', 'textarea', 'option'].includes(tagName) ? elem.value = value : elem.textContent = value;
    }
    return elem;
}

function checkList(arr, requiredProps) {
    let correctArr = []
    arr.forEach((item, index) => {
        try {
            requiredProps.forEach(prop => {
                if (!(prop in item)) {
                    throw new Error(`There is no '${prop}' key in object #${index + 1}`)
                }
            })
            correctArr.push(item)
        } catch (error) {
            console.error(error.name + ': ' + error.message);
        }
    });
    return correctArr
}

function insertList(arr, className, parentTag, childTag, insertIn) {
    try {
        let itemList = arr.map(item => {
            return Object.entries(item).map(([key, value]) => {
                return createHTMLElement(childTag, `${className}__${key}`, `${key}: ${value}`)
            })
        })
        itemList.forEach(item => {
            const div = createHTMLElement(parentTag, className)
            item.forEach(elem => div.append(elem))
            insertIn.append(div)
        })
    } catch (error) {
        console.error(error.name + ': ' + error.message);
    }
}

insertList(checkList(books, mustHaveProps), 'book', 'div', 'p', document.querySelector('#root'))

/*
function insertList(arr, className, parentTag, childTag, insertIn) {
    let itemList = arr.map(item => {
        return Object.entries(item).map(([key, value]) => {
            return createHTMLElement(childTag, `${className}__${key}`, `${key}: ${value}`)
        })
    })
    itemList.forEach(item => {
        const div = createHTMLElement(parentTag, className)
        item.forEach(elem => div.append(elem))
        insertIn.append(div)
    })
}

try {
    insertList(checkList(books, mustHaveProps), 'book', 'div', 'p', document.querySelector('#root'))
} catch (error) {
    console.error(error.name + ': ' + error.message);
}
*/

/*
function createHTMLElement(tagName = "div", className, value, attr = [], listener, event) {
    const elem = document.createElement(tagName);
    if (className) {
        elem.classList.add(className)
    }
    if (['input', 'textarea', 'option'].includes(tagName)) {
        if (value) {
            elem.value = value;
        }
        if (listener && event) {
            elem.addEventListener(event, listener)
        }
    } else {
        if (value !== undefined) {
            elem.innerHTML = value;
        }
        if (listener && event) {
            elem.addEventListener(event, listener)
        }
    }
    attr.forEach(attr => {
        if (attr instanceof Array) {
            elem.setAttribute(Object.entries(attr)[0][0], Object.entries(attr)[0][1])
        }
    })
    return elem;
}
*/

/*
function showBookList(arr) {
    const root = document.querySelector('#root');
    const mustHaveProps = ['author', 'name', 'price']

    arr.forEach((book, index) => {
        try {
            mustHaveProps.forEach(prop => {
                if (!(prop in book)) {
                    throw new Error(`There is no '${prop}' key in book #${index + 1}`)
                }
            })
            root.insertAdjacentHTML('beforeend', `
                <div class='book'>
                    <p class='book__info'>author - ${book.author}</p>
                    <p class='book__info'>name of the book - ${book.name}</p>
                    <p class='book__info'>this book cost - ${book.price}</p>
                    <hr>
                </div>
            `)
        } catch (error) {
            console.error(error.name + ': ' + error.message);
        }
    });
}

showBookList(books)
*/