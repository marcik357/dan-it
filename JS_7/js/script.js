let arr = ['hello', {}, [], 'world', 23, undefined, '23', null];
const allTypes = ['string', 'number', 'boolean', 'null', 'undefined', 'object']

function filterBy(arr, type) {
    if (type === 'object') {
        return arr.filter(item => !(typeof item === type && item !== null));
    } else if (type === 'null') {
        return arr.filter(item => item !== null);
    } else {
        return arr.filter(item => !(typeof item === type));
    }
}

allTypes.forEach(type => console.log(filterBy(arr, type)));