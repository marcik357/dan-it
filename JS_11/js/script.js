let form = document.querySelector('.password-form');

form.addEventListener('click', showPassword);
form.addEventListener('focusin', hideError);
form.addEventListener('submit', checkPassword);

function showPassword(e) {
    let password = e.target.parentElement.querySelector('.password');

    if (e.target.closest('.icon-password') && password.value) {
        if (password.type === 'password') {
            password.type = 'text';
            toggleEyeIcon(e)
        } else {
            password.type = 'password';
            toggleEyeIcon(e)
        }
    }
}

function toggleEyeIcon(e) {
    let eyeIcon = e.target.parentElement.querySelector('.icon-password');

    eyeIcon.classList.toggle('fa-eye-slash');
    eyeIcon.classList.toggle('fa-eye');
}

function checkPassword(e) {
    let passwordInputs = document.querySelectorAll('.password');
    let errorMsg = document.querySelector('.password__error');

    e.preventDefault();
    if (passwordInputs[0].value && passwordInputs[0].value === passwordInputs[1].value) {
        document.querySelectorAll('.fa-eye-slash').forEach(item => {
            item.classList.remove('fa-eye-slash');
            item.classList.add('fa-eye');
        })
        hideError();
        form.reset();
        showWellcomeMsg(e);
    } else {
        errorMsg.classList.remove('hidden');
    }
}

function hideError() {
    let errorMsg = document.querySelector('.password__error');

    errorMsg.classList.add('hidden');
}

function showWellcomeMsg(e) {
    document.body.classList.add('lock');
    document.body.insertAdjacentHTML('afterbegin', `
    <div class="wellcome">
    <div class="wellcome__message">You are welcome</div>
    </div>`);
    setTimeout(() => {
        document.querySelector('.wellcome').remove();
        document.body.classList.remove('lock');
    }, 3000);
}