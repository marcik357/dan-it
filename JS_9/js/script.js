let arr = ["Kharkiv", ["Odessa", "Lviv", "Dnieper"], "Kiev", ["Borispol", ["Borispol", "Irpin"]], "Odessa", "Lviv", "Dnieper"];

function insertList(arr, parent = document.body) {
    let list = createLi(arr).join('');
    let ul = document.createElement('ul');

    function createLi(arr) {
        return arr.map(item => {
            if (Array.isArray(item)) {
                return item = `<li><ul>${createLi(item).join('')}</ul></li>`;
            } else {
                return item = `<li>${item}</li>`;
            }
        })
    };

    ul.innerHTML = list
    parent.prepend(ul);
};

let counter = 3;
let div = document.createElement('div');

div.innerText = `Page will clear in ${counter}`;
document.body.prepend(div);

let timer = setInterval(() => {
    counter--;
    div.innerText = `Page will clear in ${counter}`;
}, 1000);

setTimeout(() => {
    clearInterval(timer);
    document.body.innerHTML = '';
}, 3000);

insertList(arr);