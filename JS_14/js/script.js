if (localStorage.getItem('theme') === 'dark') {
    document.head.insertAdjacentHTML('beforeend', `<link href="./css/dark-theme.css" rel="stylesheet">`);
}

const themeBtn = document.querySelector('#theme');

themeBtn.addEventListener('click', changeTheme);

function changeTheme(e) {
    if (e.target.closest('#theme')) {
        if (document.head.querySelector('[href*=dark-theme]')) {
            document.head.querySelector('[href*=dark-theme]').remove();
            localStorage.removeItem('theme', 'dark');
        } else {
            document.head.insertAdjacentHTML('beforeend', `<link href="./css/dark-theme.css" rel="stylesheet">`);
            localStorage.setItem('theme', 'dark');
        }
    }
}
