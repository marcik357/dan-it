import { USERS_URL, ROOT } from "./vars.js";
import { Card } from "./classes.js";

function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
}

function createHTMLElement(tagName = "div", className, value, attr = [], listener, event = 'click') {
    const elem = document.createElement(tagName);
    if (typeof className === 'string') {
        elem.classList.add(className)
    } else if (className instanceof Array) {
        className.forEach(name => elem.classList.add(name))
    }
    if (value !== undefined) {
        elem.innerHTML = value;
    }
    attr.forEach(obj => {
        if (obj instanceof Object) {
            elem.setAttribute(Object.entries(obj)[0][0], Object.entries(obj)[0][1])
        }
    })
    if (listener && event) {
        elem.addEventListener(event, listener)
    }
    return elem;
}

function showLoader(insertIn) {
    insertIn.insertAdjacentHTML('afterbegin', `
        <div class="loading">
            <div class="circle"></div>
            <div class="circle"></div>
            <div class="circle"></div>
            <div class="shadow"></div>
            <div class="shadow"></div>
            <div class="shadow"></div>
            <span>Loading</span>
        </div>
    `)
}

function showModal(type, card) {
    const modal = createHTMLElement("div", undefined, '', [{"id": "modal"}])
    modal.classList.add('active')
    const form = createHTMLElement("form", 'form')
    const inputTitle = createHTMLElement("input", 'form__input', '', [{ "type": "text" }, { "name": "title" }, { "id": "formTitle" }, { "placeholder": "Title" }, { "required": true }])
    const textarea = createHTMLElement("textarea", 'form__textarea', '', [{ "name": "body" }, { "id": "formBody" }, { "placeholder": "Some text..." }, { "required": true }])
    const formBtns = createHTMLElement("div", 'form__btns')
    if (type === 'save') {
        const saveBtn = createHTMLElement("button", ['form__btn', 'form__btn--save'], 'Save', [{ 'type': 'submit' }, { 'data-target': `${card.getId()}` }], card.saveChangedCard.bind(card))
        formBtns.append(saveBtn)
        inputTitle.value = card.getTitle()
        textarea.value = card.getBody()
    } else {
        const addBtn = createHTMLElement("button", ['form__btn', 'form__btn--add'], 'Add', [{ 'type': 'submit' }], addNewCard)
        formBtns.append(addBtn)
    }
    const cancelBtn = createHTMLElement("button", ['form__btn', 'form__btn--cancel'], 'Cancel', [{ 'type': 'reset' }], closeModal)
    form.append(inputTitle, textarea, formBtns)
    formBtns.append(cancelBtn)
    modal.append(form)
    document.body.prepend(modal)
}

function closeModal() {
    document.querySelector('#modal').remove()
}

function addNewCard(e) {
    e.preventDefault()
    const formTitle = document.querySelector('#formTitle').value
    const formBody = document.querySelector('#formBody').value
    if (formTitle.length < 2 || formBody.length < 2) return

    fetchData("https://ajax.test-danit.com/api/json/posts", {
        method: 'POST',
        body: JSON.stringify({
            userId: randomInt(1, 10),
            title: `${formTitle}`,
            body: `${formBody}`
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(({ userId, title, body, id }) => {
            fetchData(USERS_URL)
                .then(users => users.find(item => item.id === userId))
                .then(({ name, username, email }) => {
                    closeModal()
                    const card = new Card(id, userId, name, username, email, title, body)
                    document.querySelector('#addCard').after(card.showCard())
                })
        })
}

function fetchData(url, options) {
    return fetch(url, options)
        .then(response => {
            if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }
            return response.json()
        })
        .catch(error => console.error(error))
}

function fetchMultipleData(data) {
    return Promise.all(data.map((item) => fetchData(item)))
}

function createCards([users, allPosts]) {
    return users.map(({ id, name, username: nickname, email }) => {
        const userPosts = allPosts.flat().filter(post => post.userId === id)
        return userPosts.map(({ id, userId, title, body }) => {
            const card = new Card(id, userId, name, nickname, email, title, body);
            return card
        })
    }).flat()
}

function showCards(cards) {
    const addCard = createHTMLElement('button', 'addCard', 'Add new post', [{ 'id': 'addCard' }], showModal)
    ROOT.append(addCard)
    cards.forEach(card => addCard.after(card.showCard()))
}

export { createHTMLElement, showLoader, showModal, closeModal, fetchData, fetchMultipleData, createCards, showCards }