import { showLoader, fetchMultipleData, createCards, showCards } from "./functions.js";
import { USERS_URL, POSTS_URL, ROOT } from "./vars.js";

showLoader(ROOT)

fetchMultipleData([USERS_URL, POSTS_URL])
    .then(response => createCards(response))
    .then(users => {
        document.querySelector('.loading') ? document.querySelector('.loading').remove() : undefined
        showCards(users)
    })
    .catch(error => console.error(error))