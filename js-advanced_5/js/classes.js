import { createHTMLElement, showModal, closeModal, fetchData } from "./functions.js";

export class Card {
    #id
    #userId
    #name
    #nickname
    #email
    #title
    #body

    constructor(id, userId, name, nickname, email, title, body) {
        this.#id = id
        this.#userId = userId
        this.#name = name
        this.#nickname = nickname
        this.#email = email
        this.#title = title
        this.#body = body
    }

    showCard() {
        const card = createHTMLElement('div', 'card', undefined, [{ 'data-id': this.getId() }])
        card.addEventListener('click', (e) => e.target.closest('.card__edit') ? showModal('save', this) : undefined)
        card.addEventListener('click', (e) => e.target.closest('.card__del') ? this.deleteCard(this.getId()) : undefined)
        card.insertAdjacentHTML('beforeend', `
            <div class="card__header">
                <h4 class="card__author">
                    <span class="card__name">${this.getName()}</span>
                    <span class="card__nickname">${this.getNickname()}</span>
                </h4>
                <div class="card__btns">
                    <button class="card__edit" type="button">&#128393;</button>
                    <button class="card__del" type="button"></button>
                </div>
            </div>
            <p class="card__email">${this.getEmail()}</p>
            <p class="card__title">${this.getTitle()}</p>
            <p class="card__body">${this.getBody()}</p>
        `)
        return card
    }

    deleteCard(id) {
        fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
            method: 'DELETE',
        })
            .then(response => response.ok ? document.querySelector(`[data-id='${id}']`).remove() : undefined)
            .catch(error => console.error(error))
    }

    saveChangedCard(e) {
        e.preventDefault()
        const modal = e.target.closest('#modal')
        const title = modal.querySelector('#formTitle').value
        const body = modal.querySelector('#formBody').value

        fetchData(`https://ajax.test-danit.com/api/json/posts/${this.getId()}`, {
            method: 'PUT',
            body: JSON.stringify({
                title: title,
                body: body
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(({ id, title, body }) => {
                this.#title = title
                this.#body = body
                closeModal()
                const card = document.querySelector(`[data-id="${id}"]`)
                card.querySelector('.card__title').textContent = this.#title
                card.querySelector('.card__body').textContent = this.#body
            })
    }

    setId(num) {
        if (!isNaN(num)) this.#id = num
    }
    getId() {
        return this.#id
    }

    setUserId(num) {
        if (!isNaN(num)) this.#userId = num
    }
    getUserId() {
        return this.#userId
    }

    setName(str) {
        this.#name = str
    }
    getName() {
        return this.#name
    }

    setNickname(str) {
        this.#nickname = str
    }
    getNickname() {
        return this.#nickname
    }

    setEmail(str) {
        this.#email = str
    }
    getEmail() {
        return this.#email
    }

    setTitle(str) {
        this.#title = str
    }
    getTitle() {
        return this.#title
    }

    setBody(str) {
        this.#body = str
    }
    getBody() {
        return this.#body
    }
}
