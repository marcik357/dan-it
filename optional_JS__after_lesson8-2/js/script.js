function filterCollection(arr, str, boolean, ...args) {
    let keyWords = str.toLowerCase().split(' ');  // ['en_us', 'toyota', 'audi']
    let keyPath = [...args].map(key => key.indexOf('.') !== -1 ? key = key.split('.') : [key]); // [['name'], ['description'], ['contentType.name'], ['contentType.extraKey.name'], ['locales.name'], ['locales.description'], ['locales.extraKey.description']]
    
    function getKey(obj, keys) {
        if (obj && keys) {
            let key = keys.shift();
            return keys.length ? getKey(obj[key], keys) : obj[key];
        }
    }

    function getValue(obj, path) {
        if (Array.isArray(obj[path[0]])) {
            for (let item of obj[path[0]]) {
                let keys = path.slice(1);
                let findKey = getKey(item, keys);

                if (findKey !== undefined) {
                    return findKey.toLowerCase()
                }
            }
        } else if (obj[path[0]]) {
            let keys = path.slice(0);
            let findKey = getKey(obj, keys);

            if (findKey !== undefined) {
                return findKey.toLowerCase()
            }
        }
    }

    if (boolean) {
        return arr.filter(obj => {
            return keyWords.every(keyWord => {
                for (const path of keyPath) {
                    if (keyWord === getValue(obj, path)) return true;
                }
            })
        });
    } else {
        return arr.filter(obj => {
            return keyWords.some(keyWord => {
                for (const path of keyPath) {
                    if (keyWord === getValue(obj, path)) return true;
                }
            })
        });
    }

    // if (boolean) {
    //     return arr.filter(obj => {
    //         return keyWords.every(keyWord => {
    //             for (const path of keyPath) {
    //                 if (Array.isArray(obj[path[0]])) {
    //                     for (let item of obj[path[0]]) {
    //                         // let keys = path.slice(1);
    //                         // let key = getKey(item, keys);
                            
    //                         // if (key !== undefined && keyWord == key.toLowerCase()) {
    //                         //     return true
    //                         // }
    //                     }
    //                 } else if (obj[path[0]]) {
    //                     let keys = path.slice(0);
    //                     let key = getKey(obj, keys);
                        
    //                     if (key !== undefined && keyWord == key.toLowerCase()) {
    //                         return true
    //                     }
    //                 }
    //             }
    //         })
    //     });
    // } else {
    //     return arr.filter(obj => {
    //         return keyWords.some(keyWord => {
    //             for (const path of keyPath) {
    //                 if (Array.isArray(obj[path[0]])) {
    //                     for (let item of obj[path[0]]) {
    //                         let keys = path.slice(1);
    //                         let key = getKey(item, keys);
                            
    //                         if (key !== undefined) {
    //                             return keyWord == key.toLowerCase()
    //                         }
    //                     }
    //                 } else if (obj[path[0]]) {
    //                     let keys = path.slice(0);
    //                     let key = getKey(obj, keys);
                        
    //                     if (key !== undefined) {
    //                         return keyWord == key.toLowerCase()
    //                     }
    //                 }
    //             }
    //         })
    //     });
    // }
};

const vehicles = [
    {
        obj_title: 1,
        name: 'Toyota',
        description: 'en_US',
        contentType: {
            obj1_key1: 'some text1',
            name: 'Toyota',
            extraKey: {
                name: 'Audi',
                arr_obj2_key2_obj_inside_key: 'some text2',
            },
        },
        locales: [
            {
                name: 'Audi',
                arr_obj3_key1: 'some text1',
                description: 'en_US'
            },
            {
                arr_obj2_key1: 'some text2',
                extraKey: {
                    description: 'en_US',
                    arr_obj2_key2: 'some text2',
                },
            },
        ],
    },
    {
        obj_title: 2,
        name: 'Toyota',
        description: 'en_US',
        contentType: {
            obj1_key1: 'some text1',
            name: 'Toyota',
            extraKey: {
                arr_obj2_key2_obj_inside_key: 'some text2',
            },
        },
        locales: [
            {
                arr_obj3_key1: 'some text1',
                description: 'en_US'
            },
            {
                arr_obj2_key1: 'some text2',
                extraKey: {
                    description: 'en_US',
                    arr_obj2_key2: 'some text2',
                },
            },
        ],
    },
    {
        obj_title: 3,
        contentType: {
            obj1_key1: 'some text1',
            extraKey: {
                arr_obj2_key2_obj_inside_key: 'some text2',
            },
        },
        locales: [
            {
                arr_obj3_key1: 'some text1',
            },
            {
                arr_obj2_key1: 'some text2',
                extraKey: {
                    description: 'en_US',
                    arr_obj2_key2: 'some text2',
                },
            },
        ],
    },
    {
        obj_title: 4,
        locales: [
            {},
            {
                arr_obj3_key1: 'some text1',
                description: 'en_US'
            },
            {},
        ],
    },
    {
        obj_title: 5,
        name: 'ToYoTa',
        locales: [
            {},
            {
                name: 'AuDi',
                arr_obj3_key1: 'some text1',
                description: 'eN_uS'
            },
            {},
        ],
    },
];

console.log(filterCollection(vehicles, 'en_US Toyota Audi', true, 'name', 'description', 'contentType.name', 'contentType.extraKey.name', 'locales.name', 'locales.description', 'locales.extraKey.description'));
console.log(filterCollection(vehicles, 'en_US Toyota Audi', false, 'name', 'description', 'contentType.name', 'contentType.extraKey.name', 'locales.name', 'locales.description', 'locales.extraKey.description'));