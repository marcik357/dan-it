### Homework gulp-scss for DAN.IT
Based on Gulp with using SCSS
[Figma file](https://www.figma.com/file/OMxGUCGeJ18yGvPOL1awVQ/320px?node-id=0%3A1)

## Start guide
1) "npm i"
2) "npm run dev" to start developer mode or "npm run build" to create final production files (folder "dist")

## Used packeges
- browser-sync
- del
- gulp
- gulp-autoprefixer
- gulp-clean-css
- gulp-file-include
- gulp-group-css-media-queries
- gulp-if
- gulp-imagemin
- gulp-newer
- gulp-notify
- gulp-plumber
- gulp-rename
- gulp-replace
- gulp-sass
- gulp-version-number
- gulp-webp
- gulp-webp-html-nosvg
- gulp-webpcss
- sass
- webp-converter
- webpack
- webpack-stream
