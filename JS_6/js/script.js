function createNewUser() {
    let newUser = {
        getLogin() {
            return (this.firstName.substring(0, 1) + this.lastName).toLowerCase();
        },
        getAge() {
            let age = new Date().getFullYear() - new Date(birthday.slice(-4), (birthday.slice(3, 5) - 1), birthday.slice(0, 2)).getFullYear();
            let month = new Date().getMonth() - (birthday.slice(3, 5) - 1);
            if (month < 0 || (month === 0 && new Date().getDate() < birthday.slice(0, 2))) {
                age++;
            }
            return age;
        },
        getPassword() {
            return this.firstName.substring(0, 1).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
        },
        setFirstName(newFirstName) {
            Object.defineProperty(this, 'firstName', {
                value: newFirstName,
            });
        },
        setLastName(newLastName) {
            Object.defineProperty(this, 'lastName', {
                value: newLastName,
            });
        },
        setBirthday(newBirthday) {
            Object.defineProperty(this, 'birthday', {
                value: newBirthday,
            });
        },
    };

    Object.defineProperty(newUser, 'firstName', {
        value: prompt('Enter user first name'),
        writable: false,
        configurable: true
    });
    Object.defineProperty(newUser, 'lastName', {
        value: prompt('Enter user last name'),
        writable: false,
        configurable: true
    });

    let day, month, year;

    while (!day || isNaN(+day) || +day > 31 || day.length > 2) {
        day = prompt('Please, enter user day of birth in formt - "14"');
        if (day < 10) day = '0' + day;
    }
    while (!month || isNaN(+month) || +month > 12 || month.length > 2) {
        month = prompt('Please, enter user month of birth in format - "05"');
        if (month < 10) month = '0' + month;
        switch (+month) {
            case 4:
            case 6:
            case 9:
            case 11:
                while (!day || isNaN(+day) || +day > 30 || day.length > 2) {
                    day = prompt('Please, enter user day of birth in formt - "14"');
                    if (day < 10) day = '0' + day;
                }
                break;
            case 2:
                while (!day || isNaN(+day) || +day > 29 || day.length > 2) {
                    day = prompt('Please, enter user day of birth in formt - "14"');
                    if (day < 10) day = '0' + day;
                }
                break;
            default:
                break;
        }
    }
    while (!year || isNaN(+year) || new Date().getFullYear() < +year || +year < 1910 || year.length !== 4) {
        year = prompt('Please, enter user year of birth in format - "1990"');
        if (new Date(year, 1, 29).getMonth() != 1) {
            day = prompt('Please, enter user day of birth in formt - "14"');
            while (!day || isNaN(+day) || +day > 28 || day.length > 2) {
                day = prompt('Please, enter user day of birth in formt - "14"');
                if (day < 10) day = '0' + day;
            }
        }
    }

    let birthday = `${day}.${month}.${year}`;
    Object.defineProperty(newUser, 'birthday', {
        value: birthday,
        writable: false,
        configurable: true
    });

    return newUser;
};

let myUser = createNewUser();
console.log(myUser);
console.log(myUser.getLogin());
console.log(myUser.getAge());
console.log(myUser.getPassword());