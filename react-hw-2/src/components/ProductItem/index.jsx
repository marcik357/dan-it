import { Component } from "react";
import style from './productItem.module.scss'
import PropTypes from 'prop-types';
import Button from '../Button'
import { FavoriteIcon } from "../icons/FavoriteIcon";
import noimage from '../../assets/noimage.jpg'


export default class ProductItem extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isFavorite: false
        }
    }

    componentDidMount = () => this.props.favorites.find(item => item.artNum === this.props.product.artNum) && this.setState(state => ({ ...state, isFavorite: true }))

    markAsFavorite = () => this.state.isFavorite ? this.setState(state => ({ ...state, isFavorite: false })) : this.setState(state => ({ ...state, isFavorite: true }))

    render() {
        const { name, artNum, imgUrl, color, price } = this.props.product
        const { onAddToFavorite, onAddToCart } = this.props

        return (
            <a href='/' className={style.product}>
                <header className={style.product__header}>
                    <h3 className={style.product__name}>{name}</h3>
                    <div className={style.product__artNum}>{artNum}</div>
                </header>
                <div className={style.product__body}>
                    <div className={style.product__img}>
                        <img src={imgUrl || noimage} alt={name} />
                    </div>
                    <div className={style.product__descr}>
                        <div className={style.product__color}>{color}</div>
                        <div className={style.product__price}>{price} <span>грн</span></div>
                        <div className={style.product__btns}>
                            <Button
                                className={style.product__favBtn}
                                onClick={(e) => {
                                    onAddToFavorite(e, this.props.product)
                                    this.markAsFavorite()
                                }}>
                                <FavoriteIcon height={32} width={32} fill={this.state.isFavorite ? '#fc0' : '#a1bf36'} />
                            </Button>
                            <Button
                                className={style.product__buyBtn}
                                text={'Купити'}
                                onClick={(e) => onAddToCart(e, 'buy', artNum)} />
                        </div>
                    </div>
                </div>
            </a >
        )
    }
}

ProductItem.propTypes = {
    onAddToFavorite: PropTypes.func,
    favorites: PropTypes.array,
    onAddToCart: PropTypes.func,
    product: PropTypes.shape({
        name: PropTypes.string,
        artNum: PropTypes.number,
        imgUrl: PropTypes.string,
        color: PropTypes.string,
        price: PropTypes.number,
    })
}

ProductItem.defaultProps = {
    product: {
        imgUrl: noimage,
    },
}