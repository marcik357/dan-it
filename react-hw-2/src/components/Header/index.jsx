import { Component } from "react";
import style from './header.module.scss'
import PropTypes from 'prop-types';
import { FavoriteIcon, CartIcon } from "../icons";

export default class Header extends Component {

    render() {
        const { favorites, cart } = this.props

        return (
            <>
                <header className={style.header}>
                    <div className={style.header__container}>
                        <div className={style.header__wrapper}>
                            <a href='/' className={style.header__logo}>
                                <img className={style.header__img} src="./logo.png" alt="logo" />
                            </a>
                            <div className={style.header__btns}>
                                <a href='/' className={style.header__favorites}>
                                    <FavoriteIcon />
                                    {favorites > 0 && <div>{favorites}</div>}
                                </a>
                                <a href='/' className={style.header__cart}>
                                    <CartIcon />
                                    {cart > 0 && <div>{cart}</div>}
                                </a>
                            </div>
                        </div>
                    </div>
                </header>
            </>
        )
    }
}

Header.propTypes = {
    favorites: PropTypes.number,
    cart: PropTypes.number
}

Header.defaultProps = {
    favorites: 0,
    cart: 0
}