import { Component } from 'react'
import style from './modal.module.scss'
import PropTypes from 'prop-types';

export default class Modal extends Component {

    render() {
        const { type, title, description, closeBtn, closeBtnHandler, actions } = this.props.data
        const { onCancel, onSubmit } = this.props

        return (
            <div className={style.modal + ' ' + style[type]} onClick={onCancel}>
                <div className={style.modal__content} onClick={(e) => e.stopPropagation()}>
                    <header className={style.modal__header}>
                        <h3 className={style.modal__title}>
                            {title}
                        </h3>
                        {closeBtn && closeBtnHandler(onCancel, style.close)}
                    </header>
                    <div className={style.modal__body}>
                        {description && <p>{description}</p>}
                        {actions && actions(onCancel, onSubmit, style.modal__btns)}
                    </div>
                </div>
            </div>
        )
    }
}

Modal.propTypes = {
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func,
    data: PropTypes.shape({
        type: PropTypes.string,
        title: PropTypes.string,
        closeBtn: PropTypes.bool,
        closeBtnHandler: PropTypes.func,
        description: PropTypes.string,
        actions: PropTypes.func,
    })
}