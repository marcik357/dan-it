// 1)
let admin, name;

name = 'Anton';
admin = name;
console.log(admin);

// 2)
let days = 5;

days *= 86400;
console.log(days);

// 3)
let userAge = prompt('Вкажіть, будь ласка, ваш вік');

console.log(`Вам ${userAge}`);