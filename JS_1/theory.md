# JS_1


## Як можна оголосити змінну у Javascript?
```javascript

let someStr = 'string';
const someConst = 'const';

let someNewStr;
const someNewConst;

let firstStr = 'str', secondStr = 'str';
const firstConst = 'const', secondConst = 'const'

```


## У чому різниця між функцією prompt та функцією confirm?

prompt - для текстової відповіді на щось (не обов'язково щось вводити, можно просто відмовитись)
confirm - для погодження на щось, або відмови


## Що таке неявне перетворення типів? Наведіть один приклад.

це перетворення типів данних без використання спеціальних методів

```javascript

let num = 10;
let strNum = '10';
num + strNum; // = '1010'

+strNum; // = 10
num + ''; // = '10'
```
