function calcWorkTime(workSpeed, backlog, deadline) {
    let workPoints = backlog.reduce((prev, next) => prev + next);
    let dayPoints = workSpeed.reduce((prev, next) => prev + next);
    let workDays = workPoints / dayPoints;
    let term = Math.floor(deadline - new Date()) / 8.64e+7;
    let startDate = [new Date().getFullYear(), new Date().getMonth(), new Date().getDate()];
    let weekends = 0;
    
    for (let i = 0; i <= term; i++) {
        if (new Date(startDate[0], startDate[1], startDate[2] + i).getDay() === 0 ||
        new Date(startDate[0], startDate[1], startDate[2] + i).getDay() === 1) {
            weekends++;
        }
    }
    term -= weekends;

    if (workDays < term && term - workDays > 1) {
        alert(`Усі завдання будуть успішно виконані за ${Math.floor(term - workDays)} днів до настання дедлайну!`)
    } else if (workDays < term && term - workDays < 1) {
        alert(`Усі завдання будуть успішно виконані за ${Math.floor((term - workDays) * 24)} годин до настання дедлайну!`)
    } else {
        alert(`Команді розробників доведеться витратити додатково ${Math.ceil((workDays -term) * 24)} годин після дедлайну, щоб виконати всі завдання в беклозі`)
    }
}

calcWorkTime([12, 22, 16, 20], [138, 156, 270, 102, 94, 276, 206, 156, 270, 102], new Date(2022, 11, 31));
calcWorkTime([1, 2, 5, 10, 7, 8], [10, 20, 50, 100, 70, 250], new Date(2023, 0, 15));