let table = document.createElement('table');

table.innerHTML = '<tbody></tbody>';

for (let i = 0; i < 30; i++) {
    table.querySelector('tbody').insertAdjacentHTML('beforeend', '<tr></tr>');
}

table.querySelectorAll('tr').forEach(tr => {
    for (let i = 0; i < 30; i++) {
        tr.insertAdjacentHTML('beforeend', '<td></td>');
    }
})

document.body.prepend(table);

document.body.addEventListener('click', changeColor);

function changeColor(e) {
    if (e.target.closest('td')) {
        e.target.classList.toggle('black');
    } else if (!e.target.closest('table')) {
        document.querySelectorAll('.black').forEach(td => td.classList.remove('black'));
    }
}