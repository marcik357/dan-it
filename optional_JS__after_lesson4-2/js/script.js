let num1 = +prompt('Enter first number');
let num2 = +prompt('Enter second number');
let n = +prompt('Enter N number');

while (isNaN(num1) || parseInt(num1) !== num1 ||
    isNaN(num2) || parseInt(num2) !== num2) {
    alert('Please, enter Valid numbers');
    num1 = +prompt('Enter first valid number');
    num2 = +prompt('Enter second valid number');
};
while (isNaN(n) || parseInt(n) !== n) {
    alert('Please, enter Valid N number');
    n = +prompt('Enter valid N number');
};

function getFibonachiNumber(f0, f1, n) {
    if (n === 0) {
        return f0;
    } else if (n === 1) {
        return f1;
    } else if (n < 0) {
        return getFibonachiNumber(f0, f1, n + 2) - getFibonachiNumber(f0, f1, n + 1);
    } else {
        return getFibonachiNumber(f0, f1, n - 1) + getFibonachiNumber(f0, f1, n - 2);
    }
};


alert(getFibonachiNumber(num1, num2, n));