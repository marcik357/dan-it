let priceInput = document.querySelector('.price-input');

priceInput.addEventListener('focusin', addHightlight);
priceInput.addEventListener('blur', showPrice);

function addHightlight() {
    priceInput.classList.add('focus');
    priceInput.classList.remove('error');
}

function showPrice() {
    priceInput.classList.remove('focus');
    delElem(document.querySelector('.price-span'));
    delElem(document.querySelector('.price-span-error'));

    if (priceInput.value > 0) {

        priceInput.classList.add('valid')
        priceInput.insertAdjacentHTML('beforebegin', `
        <span class="price-span">
            Поточна ціна: ${parseFloat(priceInput.value)}
            <button class="cancel"></button>
        </span>`);

        document.querySelector('.cancel').addEventListener('click', () => {
            document.querySelector('.price-span').remove();
            priceInput.classList.remove('valid');
            priceInput.value = '';
        });
    } else if (priceInput.value < 0) {
        priceInput.classList.add('error');

        priceInput.classList.remove('valid');
        priceInput.insertAdjacentHTML('afterend', `
        <span class="price-span-error">
            Please enter correct price
        </span>`);
    }
}

function delElem(elem) {
    if (elem) {
        elem.remove();
    }
}