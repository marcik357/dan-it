const FIND_BY_IP = document.querySelector('#find')
const ROOT = document.querySelector('#root')
const GET_IP = 'https://api.ipify.org/?format=json'

const GET_ADDRESS = (ip) => `http://ip-api.com/json/${ip}?fields=status,message,continent,country,regionName,city,district`

FIND_BY_IP.addEventListener('click', findYou)

async function findYou() {
    try {
        ROOT.innerHTML = ''
        showLoader(ROOT)
        const ip = await fetchData(GET_IP)
        const data = await fetchData(GET_ADDRESS(ip.ip))
        showAddress(data)
    } catch (error) {
        console.error(error);
    }
}

async function fetchData(url) {
    try {
        const response = await fetch(url)
        if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
        }
        const data = await response.json()
        return data
    } catch (error) {
        console.error(error)
    }
}

function showAddress({continent, country, regionName, city, district}) {
    ROOT.innerHTML = `
        <p>You live on the continent ${continent}</p>
        <p>In the counry ${country}</p>
        <p>Your city ${city}, ${regionName}</p>
        ${district ? `<p>District ${district}</p>` : ''}
    `
}

function showLoader(insertIn) {
    insertIn.insertAdjacentHTML('afterbegin', `
        <div class="container">
                <div class="yellow"></div>
                <div class="red"></div>
                <div class="blue"></div>
                <div class="violet"></div>
        </div>
    `)
}