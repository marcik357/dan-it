import ProductList from '../../components/ProductList';
import { useSelector } from "react-redux";

export function Favorites() {


    const favorites = useSelector(state => state.favorites.favorites)
    const loading = useSelector(state => state.loading.loading)
    const error = useSelector(state => state.error.error)

    if (error) {
        return <p>Відбулась непередбаченна помилка при завантаженні, спробуйте оновити сторінку</p>;
    }

    return (
        <>
            {!loading
                ? favorites.length
                    ? <ProductList products={favorites} type={'buy'} />
                    : <p>Ви ще нічого не додали в обране</p>
                : <p>Йде завантаження...</p>}
        </>
    )
}