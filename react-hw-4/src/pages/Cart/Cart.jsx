import ProductList from '../../components/ProductList';
import { useSelector } from "react-redux";

export function Cart() {

    const cart = useSelector(state => state.cart.cart)
    const loading = useSelector(state => state.loading.loading)
    const error = useSelector(state => state.error.error)

    if (error) {
        return <p>Відбулась непередбаченна помилка при завантаженні, спробуйте оновити сторінку</p>;
    }

    const uniqueArtNums = [...new Set(cart.map(item => item.artNum))]
    const uniqueCartProducts = uniqueArtNums.map(item => cart.find(product => product.artNum === item))

    return (
        <>
            {!loading
                ? uniqueCartProducts.length
                    ? <ProductList products={uniqueCartProducts} type={'delete'} productAmount={true} />
                    : <p>Ви ще нічого не додали в кошик</p>
                : <p>Йде завантаження...</p>}
        </>
    )
}