# JS_3


## Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
Для повторного виконання однакових операцій але з різними вхідними данними.
Для послідовної взаємодії з кожним елементов масива/колекції.
В залежності від циклу можна задавати кількість ітерацій чи якусь умову
до виконання якої буде повторюватись цикл


## Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
Цикл з попередньою умовою для отримання від юзера коректних даних (while)
Для отримання факторіалу (for)

## Що таке явне та неявне приведення (перетворення) типів даних у JS?
Явне - використання спеціальних методів [Number(), String(), Boolean()]
Неявне - без використання спеціальних методів [арифметичні операції зі строковими даними в яких записані цифри,
додавання до числа пустого рядка, подвійне заперечення ...]