let userNum = prompt('Enter extreme number of range');

while (isNaN(+userNum) || parseInt(userNum) !== +userNum || +userNum < 0) {
    alert('Please, enter VALID extreme number');
    userNum = prompt('Enter extreme number of range (bigger then 0)');
}

function isMultipleFive(extremeNum) {
    let count = 0;
    for (let i = 1; i < extremeNum; i++) {
        if (i % 5 === 0) {
            count++
            console.log(i);
        }
    }
    if (!count) console.log("Sorry, no numbers");
}

isMultipleFive(userNum);