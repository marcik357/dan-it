let m = prompt('Enter first extreme number of range (smaller)');
let n = prompt('Enter second extreme number of range (bigger)');
let count = 0;

while (isNaN(+m) || parseInt(m) !== +m || +m < 0 ||
isNaN(+n) || parseInt(n) !== +n || +n < 0 || +m > +n) {
    alert('Please, enter VALID extreme numbers');
    m = prompt('Enter first extreme number of range (smaller)');
    n = prompt('Enter second extreme number of range (bigger)');
}

function isPrime(num) {
    for (let i = 2; i < num; i++) {
        if (num % i === 0) {
            return;
        }
    }
    count++
    console.log(num);
}

for (m; m <= n; m++) {
    if (m < 2) m = 2;
    isPrime(m);
}

if (!count) console.log("Sorry, no numbers");