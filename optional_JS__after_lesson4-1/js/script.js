let num = +prompt('Enter number');

while (isNaN(num) || parseInt(num) !== num || num < 1) {
    alert('Please, enter VALID numbers');
    num = prompt('Enter number', num);
}

function getFactorial(num) {
    return num === 1 ? num : num = num * getFactorial(num - 1);
}

alert(`Factorial of your number is ${getFactorial(num)}`);