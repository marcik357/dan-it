import { cleanup } from "@testing-library/react";
import { setCart } from "../../redux/actions";
import { cartReducer } from "../../redux/reducers/cartReducer";

afterEach(cleanup);

const product = {
    name: "Fostex TH610",
    price: 21280,
    imgUrl: "https://era-in-ear.com/wp-content/uploads/shop/products/origin/4c74d51dfd633243b38d6c5435b4d9bd-1024x1024.jpg",
    artNum: 11081,
    color: "brown"
}

test("should add product to cart", () => {
    const initialState = { cart: [product] };
    const dispatch = jest.fn()
    const action = setCart('buy', product.artNum, [product], []);

    action(dispatch)
    
    const newState = cartReducer(initialState, dispatch.mock.calls[0][0]);

    expect(newState.cart).toEqual([product]);
});

test("should delete product from cart", () => {
    const initialState = { cart: [] };
    const dispatch = jest.fn()
    const action = setCart('delete', product.artNum, [product], []);

    action(dispatch)
    
    const newState = cartReducer(initialState, dispatch.mock.calls[0][0]);

    expect(newState.cart).toEqual([]);
});
