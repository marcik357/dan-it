import ProductItem from "../ProductItem";
import { render, screen, cleanup, fireEvent } from "@testing-library/react"
import React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { AppearanceContext } from "../../context";
import { BrowserRouter } from "react-router-dom";
import { modalTypes } from "../../redux/types/modalTypes";
import '@testing-library/jest-dom/extend-expect';
import { artNumTypes } from "../../redux/types/artNumTypes";


afterEach(cleanup);

const product = {
    name: "Fostex TH610",
    price: 21280,
    imgUrl: "https://era-in-ear.com/wp-content/uploads/shop/products/origin/4c74d51dfd633243b38d6c5435b4d9bd-1024x1024.jpg",
    artNum: 11081,
    color: "brown"
}
const type = 'buy'
const context = 'cardView'

let mockStore = configureStore()
let store;

beforeEach(() => {
    store = mockStore({
        cart: { cart: [] }, favorites: { favorites: [] }, artNum: { artNum: '' }, modal: { modal: '' }, products: { products: [] }
    })
})


test('snapshot ProductItem component', () => {
    const component = renderer.create(
        <Provider store={store}>
            <BrowserRouter >
                <AppearanceContext.Provider value={context}>
                    <ProductItem product={product} type={type} />
                </AppearanceContext.Provider>
            </BrowserRouter>
        </Provider>
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

test('renders ProductItem component and click buy', () => {
    render(
        <Provider store={store}>
            <BrowserRouter >
                <AppearanceContext.Provider value={context}>
                    <ProductItem product={product} type={type} />
                </AppearanceContext.Provider>
            </BrowserRouter>
        </Provider>
    );

    const button = screen.getByText('Купити')
    expect(button).toBeInTheDocument()
    fireEvent.click(button)
    const actions = store.getActions()

    expect(actions).toEqual([
        { type: modalTypes.SHOW_MODAL, payload: { modal: type } },
        { type: artNumTypes.SET_ART_NUM, payload: { artNum: product.artNum } }
    ])
});