document.body.addEventListener('click', showInput);
document.body.addEventListener('click', createPlayground);
document.body.addEventListener('click', deleteCircles);

function showInput(e) {
    if (e.target.closest('#drawCircle')) {
        const startBtn = document.querySelector('#drawCircle');

        startBtn.insertAdjacentHTML('afterend', `
            <div>
                <input type="number" name="numInput" id="numInput" placeholder='Введіть діаметр кола'>
                <button id="draw">Намалювати</button>
            </div>
        `);
        startBtn.remove();
    }
}

function createPlayground(e) {
    if (e.target.closest('#draw') && numInput.value) {
        if (document.querySelector('.playGround')) {
            document.querySelector('.playGround').remove();
        }

        let playGround = document.createElement('div');

        playGround.classList.add('playGround');
        document.querySelector('#draw').parentElement.after(playGround);
        for (let i = 0; i < 10; i++) {
            let row = document.createElement('div');

            for (let i = 0; i < 10; i++) {
                row.append(drawCircle());
            }
            playGround.append(row);
        }
    }
}

function drawCircle() {
    const circle = document.createElement('div');
    circle.classList.add('circle');
    circle.style.height = numInput.value + 'px';
    circle.style.width = numInput.value + 'px';
    circle.style.backgroundColor = getRandomColor();
    return circle;
}

function getRandomColor() {
    const letters = "0123456789ABCDEF";
    let color = '#'
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function deleteCircles(e) {
    if (e.target.closest('.circle')) e.target.remove()
}