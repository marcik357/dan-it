let student = {
    name: '',
    lastName: '',
};

function fillStudentData(obj, key, message) {
    while (!obj[key]) {
        obj[key] = prompt(message);
        if (obj[key] !== null) obj[key] = obj[key].trim();
    }
    return obj[key]
}

fillStudentData(student, 'name', 'Введіть ім\'я студента');
fillStudentData(student, 'lastName', 'Введіть прізвище студента');

function fillStudentTabel(student) {
    let subject;
    student.tabel = {};

    while (subject !== null && isNaN(subject)) {
        subject = prompt('Введіть назву предмета');
        if (subject !== null) {
            while (isNaN(+student.tabel[subject]) || parseInt(student.tabel[subject]) !== +student.tabel[subject] || +student.tabel[subject] < 0) {
                student.tabel[subject] = prompt('Введіть оцінку за предмет');
            }
        }
    }
};

fillStudentTabel(student);

function calcStudySuccess(student) {
    let badMarks = 0;
    let sum = 0;
    let subjectCount = 0;

    for (let subject in student.tabel) {
        if (student.tabel[subject] < 4) badMarks++;
        sum += student.tabel[subject];
        subjectCount++;
    }

    if (Object.keys(student.tabel).length) {
        if (!badMarks) {
            alert('Студента переведено на наступний курс');
            if (sum / subjectCount > 7) {
                alert('Студенту призначено стипендію');
            }
        }
        else {
            alert('Він йде на пересдачу');
        }
    } else {
        alert('В табелі ще не стоїть жодної оцінки');
    }
};

calcStudySuccess(student);