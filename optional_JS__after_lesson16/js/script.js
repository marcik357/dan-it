let btnNext = document.querySelector('.btn-next');
let btnPrev = document.querySelector('.btn-prev');
let translateX = 0;
let images = document.querySelector('.images');
// клонування крайніх слайдів
let cloneLast = images.children[images.children.length - 1].cloneNode(true)
let cloneFirst = images.children[0].cloneNode(true)
images.prepend(cloneLast)
images.append(cloneFirst)
// необхідне для виявлення переходу на крайній слайд
let x = document.querySelector('.slider').getBoundingClientRect().x;
let y = document.querySelector('.slider').getBoundingClientRect().y;
// обробники кнопок
btnNext.addEventListener('click', showNextImg);
btnPrev.addEventListener('click', showPrevImg);

function showNextImg(e) {
    if (e.target.closest('.btn-next')) {
        let translate = setInterval(() => {
            images.style.translate = `${translateX -= 5}%`;
            btnNext.disabled = true;
            if (translateX % 100 === 0) {
                clearInterval(translate);
                setTimeout(() => {
                    btnNext.disabled = null;
                }, 300);
            }
        }, 10);
        if (document.elementFromPoint(x, y) === images.children[images.children.length - 1]) {
            translateX = 0;
        }
    }
}
function showPrevImg(e) {
    if (e.target.closest('.btn-prev')) {
        let translate = setInterval(() => {
            images.style.translate = `${translateX += 5}%`;
            btnPrev.disabled = true;
            if (translateX % 100 === 0) {
                clearInterval(translate);
                setTimeout(() => {
                    btnPrev.disabled = null;
                }, 300);
            }
        }, 10);
        if (document.elementFromPoint(x, y) === images.children[1]) {
            translateX = parseInt(`-${images.children.length - 2}00`);
        }
    }
}