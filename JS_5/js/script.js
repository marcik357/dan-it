function createNewUser() {
    let newUser = {
        getLogin() {
            return (this.firstName.substring(0, 1) + this.lastName).toLowerCase();
        },
        setFirstName(newFirstName) {
            Object.defineProperty(this, 'firstName', {
                value: newFirstName,
            });
        },
        setLastName(newLastName) {
            Object.defineProperty(this, 'lastName', {
                value: newLastName,
            });
        }
    };

    Object.defineProperty(newUser, 'firstName', {
        value: prompt('Enter user first name'),
        writable: false,
        configurable: true
    });
    Object.defineProperty(newUser, 'lastName', {
        value: prompt('Enter user last name'),
        writable: false,
        configurable: true
    });
    
    return newUser;
};

let myUser = createNewUser();
console.log(myUser.getLogin());